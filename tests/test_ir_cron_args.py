# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class IrCronArgsTestCase(ModuleTestCase):
    """Test Ir Cron Args module"""
    module = 'ir_cron_args'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            IrCronArgsTestCase))
    return suite
